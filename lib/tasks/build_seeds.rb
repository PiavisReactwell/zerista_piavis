namespace :events do 
	desc "creates the seeds.rb file from the test_events.csv file"
	task :create_seeds, [:env] => [:environment] do

	junk_first_line = true
	CSV.foreach("test_events.csv") do |line|
		unless junk_first_line
			puts line[0]
		end
		junk_first_line = false
	end
end