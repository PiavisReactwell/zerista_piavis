# Tool for building seeds from test_events csv
# [0] = start time, [1] = end time, 2 = name, 3 = track

require 'csv'

namespace :events do 
	desc "creates the seeds.rb file from the test_events.csv file"
	task :create_seeds, [:env] => [:environment] do

		junk_first_line = true
		CSV.foreach("lib/assets/test_events.csv") do |line|
			unless junk_first_line
				# Build the day if it does not exist.
				day = Day.find_by(date: Date.strptime(line[0], '%m/%d/%Y'))
				if day.nil?
					day = Day.create!(date: Date.strptime(line[0], '%m/%d/%Y'), day_of_week: Date.strptime(line[0], '%m/%d/%Y').to_date.strftime("%A"))
				end

				# Build track if it does not exist.
				track = Track.find_by(name: line[3].chomp)
				if track.nil?
					# The track does not exist, build it.
					has_column = line[3].include?("Track")
					position = Track.last ? Track.last.position + 1 : 1
					track = Track.create!(name: line[3].chomp, position: position, color: '#ff0', has_column: has_column)
				end
				begin
					track.days << day
				rescue ActiveRecord::RecordNotUnique => e
					next
				end
				spans = line[2].include?('Plenary') || line[2].include?('Coffee Break') || line[2].include?('Lunch') || line[2].include?('acquarium')
				event = Event.create!(start_time: DateTime.strptime(line[0], '%m/%d/%Y %H:%M'), end_time: DateTime.strptime(line[1], '%m/%d/%Y %H:%M'), name: line[2], track_id: track.id, spans_all_tracks: spans, day_id: day.id)
			end
			junk_first_line = false
		end
	end
end