# README

Thanks for your consideration Charlie.

Went with the default SQLite3 database option.

There is a rake file which loads the data into your database.

`bundle install`
`bundle exec rake db:migrate;rake events:create_seeds`
`rails s`

Should be running on a puma server at :3000

The home page should be the "Welcome to the conference."

Saturday 2009-11-07 only had the "Courses" track which I assume should have been an all day event.
However, I never put in the `|| has_column = line[3].include?("Track")` in the build_seeds.rake to give it a column.  This could be fixed in the rake task or the database to give the "courses" track a column.

Tuesday 2015-12-08 displays all of the table headers, none of the data, the table cells were the next thing to build when I ran out of time.

Wendesday 2015-12-09 only has headers for 2 of the tracks, the has_and_belongs_to_many table should have had entries for the track / day, but never got to track down why it did not.

I think my prediction was fairly on point for the time it should take me to get an MVP done.

Next steps would have been to create the mxn table and then populate/color/stylize the cells with JS.