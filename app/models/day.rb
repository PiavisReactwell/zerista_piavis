class Day < ApplicationRecord

	has_many :events, -> { order "start_time"}
	has_and_belongs_to_many :tracks#, -> { order "position" }

	def last_event
		self.events.order(:end_time).last
	end

	# Returns an m x n size for the dimensions of the events table.
	def table_dimensions
		m = (self.last_event.end_time - self.events.first.start_time).to_i / 60 / 15
		n = self.tracks.where(has_column: true).count
		[m, n]
	end
end
