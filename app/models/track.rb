class Track < ApplicationRecord

	has_and_belongs_to_many :days
	has_many :events

end
