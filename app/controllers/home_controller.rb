# The home controller is going to control the index views for the given days
class HomeController < ApplicationController

	def home
		@days = Day.order(:date)
		@selected_date = get_date[:date].nil? ? @days.first.date.strftime("%Y-%m-%d") : get_date[:date]
		@day = Day.find_by(date: @selected_date)
	end

	private

		# Never trust the internet.
		def get_date
			params.permit(:date)
		end

end
