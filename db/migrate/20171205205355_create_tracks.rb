class CreateTracks < ActiveRecord::Migration[5.1]
	def change
		create_table :tracks do |t|
			t.string :name, index: true
			t.integer :position
			t.string :color
			t.boolean :has_column, default: false
			t.integer :day_id, index: true, foreign_key: true
			t.timestamps
		end
	end
end