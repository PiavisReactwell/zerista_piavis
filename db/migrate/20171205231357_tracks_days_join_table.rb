class TracksDaysJoinTable < ActiveRecord::Migration[5.1]
  def change
  	create_table :days_tracks, id: false do |t|
  		t.belongs_to :day, index: true
  		t.belongs_to :track, index: true
  	end
  	add_index :days_tracks, [:day_id, :track_id], unique: true
  end
end
