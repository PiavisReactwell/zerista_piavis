class CreateDays < ActiveRecord::Migration[5.1]
	def change
		create_table :days do |t|
			t.date :date
			t.string :day_of_week, default: "Noneday"
			t.timestamps
		end
	end
end