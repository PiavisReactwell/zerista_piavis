class CreateEvents < ActiveRecord::Migration[5.1]
	def change
	create_table :events do |t|
			t.string :name
			t.datetime :start_time, index: true
			t.datetime :end_time, index: true
			t.integer :track_id, index: true, foreign_key: true
			t.boolean :spans_all_tracks, default: false
			t.integer :day_id, index: true, foreign_key: true
			t.timestamps
		end
	end
end